module.exports = function(grunt) {
  var srcDir = 'src/scss/';

  var cssSrcList = {
    'build/x-grid.css': srcDir + 'style.scss'
  };

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    watch: {
      scripts: {
        files: [srcDir + '**/*.scss'],
        tasks: ['default'],
        options: {
        }
      }
    },
    sass: {
      dev: {
        options: {
          sourcemap: 'none',
          lineNumbers: true,
          unixNewlines: true,
        },
        files: {
          'build/x-grid.css': srcDir + 'style.scss'
        }
      },
      dist: {
        options: {
          sourcemap: 'none',
          lineNumbers: false,
          unixNewlines: true,
          style: 'compressed'
        },
        files: {
          'build/x-grid.min.css': srcDir + 'style.scss'
        }
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');

  // Default task(s).
  grunt.registerTask('default', [
    'sass:dev',
    'sass:dist'
  ]);
};
